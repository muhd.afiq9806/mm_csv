import pandas as pd
import argparse
import os

parser = argparse.ArgumentParser(description='Edit mobile mapping CSV file.')
parser.add_argument(
        '-c', '--csvFile',
        help='Path to the csv file.',
        type=str,
        default=os.getcwd()
)
parser.add_argument(
        '-f', '--fileName',
        help='The file naming.',
        type=str,
        default=os.getcwd()
)
parser.add_argument(
        '-d', '--dirname',
        help='The directory naming.',
        type=str,
        default=os.getcwd()
)

args = parser.parse_args()

#Declare variable from argparse
csvFile = args.csvFile
fileName = args.fileName
dirName = args.dirname
# x = csvFile.split(".")
df = pd.read_csv(csvFile)

df.columns

# Rename the column name respective to the reference given to upload into QGIS
df = df.rename(columns = {'GPSLongitude' :'coordx', 'GPSLatitude':'coordy', 'PoseHeadingDegrees':'heading', 'FileName':'imgname', 'GPSAltitude':'altitude', 'CreateDate':'stamp'}, inplace = False)

# Counter for adding the Frame column
frame=[]
num_col = df.shape
# print(num_col[0])
for i in range(num_col[0]):
    i+=1
    # print(i)
    frame.append(i)
# print(frame)

# Reorganize the column
df.insert(0,column="frame",value=frame)
df.drop('SourceFile', inplace=True, axis=1)
df.insert(1,column="gid",value=" ")
df.insert(4,column="dirname",value=dirName)
df.insert(5,column="filename",value=fileName)
df.insert(9,column="label",value=" ")
df.insert(10,column="durum",value=" ")
df.insert(11,column="pitch",value=" ")
df.insert(12,column="roll",value=" ")

# Modify the string 
df['coordx'] = df['coordx'].str.replace(r"'", '')
df['coordy'] = df['coordy'].str.replace(r"'", '')
df['coordx'] = df['coordx'].str.replace(r"E", '')
df['coordy'] = df['coordy'].str.replace(r"N", '')
df['altitude'] = df['altitude'].str.replace(r' m Above Sea Level', '')
df['altitude'] = df['altitude'].str.replace(r' m Below Sea Level', '')
df['gid'] = df['gid'].str.replace(r' ', '')
df['durum'] = df['durum'].str.replace(r' ', '')
df['roll'] = df['roll'].str.replace(r' ', '0')
df['pitch'] = df['pitch'].str.replace(r' ', '0')

new_fileName = fileName+"_edit.csv"
print("Done! Filename is "+new_fileName)
df.to_csv(new_fileName, header=True, index=False)
